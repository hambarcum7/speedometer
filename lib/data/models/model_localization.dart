enum Language {
  russian,
  English,
  Belarus,
  Ukraine,
  Deutsch,
}

class ModelLocalization {
  var supportedLocales = ['ru', 'en', 'ru_BY', 'uk_UA', 'de'];
  int defaultLanguage = 0;
  Language language = Language.russian;
}
