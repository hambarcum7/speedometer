//import 'package:speedometer/data/models/model_localization.dart';

enum SpeedUnit { km_h, miles_h }

class ModelSpeedometer {
  int speed = 0;
  int maxSpeed = 0;
  int averageSpeed = 0;
  int tripMeter = 0;
  DateTime time = DateTime.now();
  SpeedUnit speedUnit = SpeedUnit.km_h;
  bool hugOn = true;
  int indexLanguage = 0;
}
