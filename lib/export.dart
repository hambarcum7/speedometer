export 'package:flutter/material.dart';
export 'package:flutter_svg/flutter_svg.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:flutter_localizations/flutter_localizations.dart';
export 'package:provider/provider.dart';

export 'package:speedometer/generated/l10n.dart';
export 'package:speedometer/generated/l10n.dart';
export 'package:speedometer/path/path.dart';
export 'package:speedometer/theme/theme.dart';

export 'package:speedometer/ui/screen_buy/ScreenBuy.dart';
export 'package:speedometer/ui/screen_hud/screen_hud.dart';
export 'package:speedometer/ui/screen_speed/screen_speed.dart';
export 'package:speedometer/ui/screen_splash/screen_splash.dart';
export 'package:speedometer/ui/screen_language/screen_language.dart';
export 'package:speedometer/ui/screen_setings/screen_settings.dart';
export 'package:speedometer/ui/widgets_common/wrapper_max_with_In_landscape.dart';
export 'package:speedometer/ui/widgets_common/wrapper_button.dart';
export 'package:speedometer/ui/widgets_common/wrapper_adb.dart';
export 'package:speedometer/ui/widgets_common/wrapper_app_bar.dart';
export 'package:speedometer/ui/widgets_common/single_child_scroll_view_landscape.dart';
export 'package:speedometer/ui/widgets_common/speed_widget.dart';
export 'package:speedometer/provider/provider_localization.dart';
export 'package:speedometer/ui/screen_speed/wrapper_list_tile.dart';
export 'package:speedometer/ui/widgets_common/button_white.dart';
export 'package:speedometer/ui/widgets_common/time_widget.dart';
