// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "adsOff": MessageLookupByLibrary.simpleMessage("ADS aus"),
        "averageSpeed": MessageLookupByLibrary.simpleMessage("Durchschnitt"),
        "belarus": MessageLookupByLibrary.simpleMessage("Weißrussland"),
        "buttonBuyFull": MessageLookupByLibrary.simpleMessage(
            "Vollversionen kaufen (ADS aus)"),
        "buttonRate": MessageLookupByLibrary.simpleMessage("Bewerte 5 Sterne"),
        "buyDescription": MessageLookupByLibrary.simpleMessage(
            "Sie können Ihr Abonnement jederzeit unter \'Einstellungen\'> Apple ID kündigen, jedoch mindestens einen Tag vor dem Verlängerungsdatum. \n\nDas Abonnement verlängert sich automatisch, bis Sie es kündigen"),
        "buyFullVersion":
            MessageLookupByLibrary.simpleMessage("Vollversion kaufen"),
        "defaultLanguage": MessageLookupByLibrary.simpleMessage("Sprache"),
        "deutsch": MessageLookupByLibrary.simpleMessage("Deutsch"),
        "english": MessageLookupByLibrary.simpleMessage("Englisch"),
        "hud": MessageLookupByLibrary.simpleMessage("HUD"),
        "hudOff": MessageLookupByLibrary.simpleMessage("HUD aus"),
        "hudOn": MessageLookupByLibrary.simpleMessage("HUD an"),
        "hudView": MessageLookupByLibrary.simpleMessage("HUD-Ansicht"),
        "kilometersH": MessageLookupByLibrary.simpleMessage("Kilometer / h"),
        "kmH": MessageLookupByLibrary.simpleMessage("km / h"),
        "language": MessageLookupByLibrary.simpleMessage("Sprache"),
        "loading": MessageLookupByLibrary.simpleMessage("Laden .."),
        "maxSpeed": MessageLookupByLibrary.simpleMessage("Maximale Geschwin"),
        "milesH": MessageLookupByLibrary.simpleMessage("Meilen / h"),
        "rateFiveStars":
            MessageLookupByLibrary.simpleMessage("Bewerte 5 Sterne"),
        "reset": MessageLookupByLibrary.simpleMessage("Zurücksetzen"),
        "russian": MessageLookupByLibrary.simpleMessage("Russisch"),
        "saveChanges":
            MessageLookupByLibrary.simpleMessage("Änderungen speichern"),
        "settings": MessageLookupByLibrary.simpleMessage("Einstellungen"),
        "speedUnit":
            MessageLookupByLibrary.simpleMessage("Geschwindigkeitseinheit"),
        "speedometer":
            MessageLookupByLibrary.simpleMessage("Geschwindigkeitsmesser"),
        "termsOfService": MessageLookupByLibrary.simpleMessage(
            "Nutzungsbedingungen und Pryvisy-Richtlinie"),
        "time": MessageLookupByLibrary.simpleMessage("Zeit"),
        "tripMeter":
            MessageLookupByLibrary.simpleMessage("Tageskilometerzähler"),
        "tripTime": MessageLookupByLibrary.simpleMessage("Reisezeit"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Ukraine"),
        "version": MessageLookupByLibrary.simpleMessage("ver. 0.0.0.1")
      };
}
