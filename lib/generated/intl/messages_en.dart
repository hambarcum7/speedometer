// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "adsOff": MessageLookupByLibrary.simpleMessage("ADS off"),
        "averageSpeed": MessageLookupByLibrary.simpleMessage("Average Speed"),
        "belarus": MessageLookupByLibrary.simpleMessage("Belarus"),
        "buttonBuyFull":
            MessageLookupByLibrary.simpleMessage("Buy Full Versions (ADS off)"),
        "buttonRate": MessageLookupByLibrary.simpleMessage("Rate 5 stars"),
        "buyDescription": MessageLookupByLibrary.simpleMessage(
            "You can cancel your subscription in \'Settings\'> Apple ID at any time, but at least one day before the renewal date. \n\nThe subscription will automatically renew until you cancel it"),
        "buyFullVersion":
            MessageLookupByLibrary.simpleMessage("Buy Full Version"),
        "defaultLanguage": MessageLookupByLibrary.simpleMessage("English"),
        "deutsch": MessageLookupByLibrary.simpleMessage("Deutsch"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "hud": MessageLookupByLibrary.simpleMessage("HUD"),
        "hudOff": MessageLookupByLibrary.simpleMessage("HUD off"),
        "hudOn": MessageLookupByLibrary.simpleMessage("HUD on"),
        "hudView": MessageLookupByLibrary.simpleMessage("HUD view"),
        "kilometersH": MessageLookupByLibrary.simpleMessage("kilometers / h"),
        "kmH": MessageLookupByLibrary.simpleMessage("km / h"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "loading": MessageLookupByLibrary.simpleMessage("Loading.."),
        "maxSpeed": MessageLookupByLibrary.simpleMessage("Max Speed"),
        "milesH": MessageLookupByLibrary.simpleMessage("miles / h"),
        "rateFiveStars": MessageLookupByLibrary.simpleMessage("Rate 5 stars"),
        "reset": MessageLookupByLibrary.simpleMessage("Reset"),
        "russian": MessageLookupByLibrary.simpleMessage("Russian"),
        "saveChanges": MessageLookupByLibrary.simpleMessage("save changes"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "speedUnit": MessageLookupByLibrary.simpleMessage("Speed Unit"),
        "speedometer": MessageLookupByLibrary.simpleMessage("Speedometer"),
        "termsOfService": MessageLookupByLibrary.simpleMessage(
            "Terms of Service and Pryvisy Policy"),
        "time": MessageLookupByLibrary.simpleMessage("Time"),
        "tripMeter": MessageLookupByLibrary.simpleMessage("Trip Meter"),
        "tripTime": MessageLookupByLibrary.simpleMessage("Trip Time"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Ukraine"),
        "version": MessageLookupByLibrary.simpleMessage("ver. 0.0.0.1")
      };
}
