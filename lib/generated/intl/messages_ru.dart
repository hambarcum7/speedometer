// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "adsOff": MessageLookupByLibrary.simpleMessage("Рекламма отключена"),
        "averageSpeed":
            MessageLookupByLibrary.simpleMessage("Средняя скорость"),
        "belarus": MessageLookupByLibrary.simpleMessage("Беларусский"),
        "buttonBuyFull": MessageLookupByLibrary.simpleMessage(
            "Купить полную версию (без рекламы)"),
        "buttonRate": MessageLookupByLibrary.simpleMessage("Оценить в 5 звезд"),
        "buyDescription": MessageLookupByLibrary.simpleMessage(
            "Отменить подписку можно в разделе \'Настройки\' > Apple ID в любой момент, но не менее чем за день до даты возобновления. \n\nПодписка будет возобновляться автоматически, пока Вы ее не отмените"),
        "buyFullVersion":
            MessageLookupByLibrary.simpleMessage("Купить полную версию"),
        "defaultLanguage": MessageLookupByLibrary.simpleMessage("Русский"),
        "deutsch": MessageLookupByLibrary.simpleMessage("Немецкий"),
        "english": MessageLookupByLibrary.simpleMessage("Английский"),
        "hud": MessageLookupByLibrary.simpleMessage("HUD"),
        "hudOff": MessageLookupByLibrary.simpleMessage("HUD выключен"),
        "hudOn": MessageLookupByLibrary.simpleMessage("HUD включен"),
        "hudView": MessageLookupByLibrary.simpleMessage("Просмотр HUD"),
        "kilometersH": MessageLookupByLibrary.simpleMessage("км / ч"),
        "kmH": MessageLookupByLibrary.simpleMessage("км / ч"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "loading": MessageLookupByLibrary.simpleMessage("Загрузка .."),
        "maxSpeed":
            MessageLookupByLibrary.simpleMessage("Максимальная скорость"),
        "milesH": MessageLookupByLibrary.simpleMessage("миль / ч"),
        "rateFiveStars":
            MessageLookupByLibrary.simpleMessage("Оценить 5 звезд"),
        "reset": MessageLookupByLibrary.simpleMessage("Сброс"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "saveChanges":
            MessageLookupByLibrary.simpleMessage("Сохранить изменения"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "speedUnit": MessageLookupByLibrary.simpleMessage("Единица скорости"),
        "speedometer": MessageLookupByLibrary.simpleMessage("Спидометр"),
        "termsOfService": MessageLookupByLibrary.simpleMessage(
            "Условия использования и Политика конфиденциальности"),
        "time": MessageLookupByLibrary.simpleMessage("Время"),
        "tripMeter": MessageLookupByLibrary.simpleMessage("Счетчик пробега"),
        "tripTime": MessageLookupByLibrary.simpleMessage("Время в пути"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Украинский"),
        "version": MessageLookupByLibrary.simpleMessage("ver. 0.0.0.1")
      };
}
