// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_BY locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_BY';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "adsOff": MessageLookupByLibrary.simpleMessage("Рекламма адключаная"),
        "averageSpeed":
            MessageLookupByLibrary.simpleMessage("Сярэдняя хуткасць"),
        "belarus": MessageLookupByLibrary.simpleMessage("Беларускі"),
        "buttonBuyFull": MessageLookupByLibrary.simpleMessage(
            "Купіць поўную версію (без рэкламы)"),
        "buttonRate": MessageLookupByLibrary.simpleMessage("Ацаніць ў 5 зорак"),
        "buyDescription": MessageLookupByLibrary.simpleMessage(
            "Адмяніць падпіску можна ў раздзеле \'Налады\'> Apple ID ў любы момант, але не менш чым за дзень да даты аднаўлення. \nПодписка будзе аднаўляцца аўтаматычна, пакуль Вы яе ня адмяніце"),
        "buyFullVersion":
            MessageLookupByLibrary.simpleMessage("Купіць поўную версію"),
        "defaultLanguage": MessageLookupByLibrary.simpleMessage("беларускі"),
        "deutsch": MessageLookupByLibrary.simpleMessage("Нямецкі"),
        "english": MessageLookupByLibrary.simpleMessage("Англійская"),
        "hud": MessageLookupByLibrary.simpleMessage("HUD"),
        "hudOff": MessageLookupByLibrary.simpleMessage("HUD выключаны"),
        "hudOn": MessageLookupByLibrary.simpleMessage("HUD уключаны"),
        "hudView": MessageLookupByLibrary.simpleMessage("Прагляд HUD"),
        "kilometersH": MessageLookupByLibrary.simpleMessage("Км / г"),
        "kmH": MessageLookupByLibrary.simpleMessage("Км / г"),
        "language": MessageLookupByLibrary.simpleMessage("Мова"),
        "loading": MessageLookupByLibrary.simpleMessage("Загрузка .."),
        "maxSpeed":
            MessageLookupByLibrary.simpleMessage("Максімальная хуткасць"),
        "milesH": MessageLookupByLibrary.simpleMessage("Міль / ч"),
        "rateFiveStars":
            MessageLookupByLibrary.simpleMessage("Ацаніць 5 зорак"),
        "reset": MessageLookupByLibrary.simpleMessage("Скід"),
        "russian": MessageLookupByLibrary.simpleMessage("Рускі"),
        "saveChanges": MessageLookupByLibrary.simpleMessage("Захаваць зьмены"),
        "settings": MessageLookupByLibrary.simpleMessage("Налады"),
        "speedUnit": MessageLookupByLibrary.simpleMessage("Адзінка хуткасці"),
        "speedometer": MessageLookupByLibrary.simpleMessage("Спідометр"),
        "termsOfService": MessageLookupByLibrary.simpleMessage(
            "Умовы выкарыстання і Палітыка прыватнасці"),
        "time": MessageLookupByLibrary.simpleMessage("Час"),
        "tripMeter": MessageLookupByLibrary.simpleMessage("Лічыльнік прабегу"),
        "tripTime": MessageLookupByLibrary.simpleMessage("Час у шляху"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Украінскі"),
        "version": MessageLookupByLibrary.simpleMessage("ver. 0.0.0.1")
      };
}
