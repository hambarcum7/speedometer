// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a uk_UA locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'uk_UA';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "adsOff": MessageLookupByLibrary.simpleMessage("Рекламма відключена"),
        "averageSpeed":
            MessageLookupByLibrary.simpleMessage("Середня швидкість"),
        "belarus": MessageLookupByLibrary.simpleMessage("Білоруський"),
        "buttonBuyFull": MessageLookupByLibrary.simpleMessage(
            "Купити повну версію (без реклами)"),
        "buttonRate": MessageLookupByLibrary.simpleMessage("Оцінити в 5 зірок"),
        "buyDescription": MessageLookupByLibrary.simpleMessage(
            "Скасувати підписку можна в розділі \'Налаштування\'> Apple ID в будь-який момент, але не менше ніж за день до дати поновлення. \n \nПодпіска буде поновлюватися автоматично, поки Ви її не скасуєте"),
        "buyFullVersion":
            MessageLookupByLibrary.simpleMessage("Купити повну версію"),
        "defaultLanguage": MessageLookupByLibrary.simpleMessage("Українська"),
        "deutsch": MessageLookupByLibrary.simpleMessage("Німецька"),
        "english": MessageLookupByLibrary.simpleMessage("Англійська"),
        "hud": MessageLookupByLibrary.simpleMessage("HUD"),
        "hudOff": MessageLookupByLibrary.simpleMessage("HUD вимкнений"),
        "hudOn": MessageLookupByLibrary.simpleMessage("HUD включений"),
        "hudView": MessageLookupByLibrary.simpleMessage("Перегляд HUD"),
        "kilometersH": MessageLookupByLibrary.simpleMessage("Км / год"),
        "kmH": MessageLookupByLibrary.simpleMessage("Км / год"),
        "language": MessageLookupByLibrary.simpleMessage("Мова"),
        "loading": MessageLookupByLibrary.simpleMessage("Завантаження .."),
        "maxSpeed":
            MessageLookupByLibrary.simpleMessage("Максимальна швидкість"),
        "milesH": MessageLookupByLibrary.simpleMessage("Миль / год"),
        "rateFiveStars":
            MessageLookupByLibrary.simpleMessage("Оцінити 5 зірок"),
        "reset": MessageLookupByLibrary.simpleMessage("Скидання"),
        "russian": MessageLookupByLibrary.simpleMessage("Російська"),
        "saveChanges": MessageLookupByLibrary.simpleMessage("Зберегти зміни"),
        "settings": MessageLookupByLibrary.simpleMessage("Налаштування"),
        "speedUnit": MessageLookupByLibrary.simpleMessage("Одиниця швидкості"),
        "speedometer": MessageLookupByLibrary.simpleMessage("Спідометр"),
        "termsOfService": MessageLookupByLibrary.simpleMessage(
            "Умови використання та Політика конфіденційності"),
        "time": MessageLookupByLibrary.simpleMessage("Час"),
        "tripMeter": MessageLookupByLibrary.simpleMessage("Лічильник пробігу"),
        "tripTime": MessageLookupByLibrary.simpleMessage("Час у дорозі"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Україньська"),
        "version": MessageLookupByLibrary.simpleMessage("Ver. 0.0.0.1")
      };
}
