// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Max Speed`
  String get maxSpeed {
    return Intl.message(
      'Max Speed',
      name: 'maxSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Average Speed`
  String get averageSpeed {
    return Intl.message(
      'Average Speed',
      name: 'averageSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Trip Meter`
  String get tripMeter {
    return Intl.message(
      'Trip Meter',
      name: 'tripMeter',
      desc: '',
      args: [],
    );
  }

  /// `Trip Time`
  String get tripTime {
    return Intl.message(
      'Trip Time',
      name: 'tripTime',
      desc: '',
      args: [],
    );
  }

  /// `Time`
  String get time {
    return Intl.message(
      'Time',
      name: 'time',
      desc: '',
      args: [],
    );
  }

  /// `Reset`
  String get reset {
    return Intl.message(
      'Reset',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `HUD`
  String get hud {
    return Intl.message(
      'HUD',
      name: 'hud',
      desc: '',
      args: [],
    );
  }

  /// `Speedometer`
  String get speedometer {
    return Intl.message(
      'Speedometer',
      name: 'speedometer',
      desc: '',
      args: [],
    );
  }

  /// `Loading..`
  String get loading {
    return Intl.message(
      'Loading..',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Speed Unit`
  String get speedUnit {
    return Intl.message(
      'Speed Unit',
      name: 'speedUnit',
      desc: '',
      args: [],
    );
  }

  /// `km / h`
  String get kmH {
    return Intl.message(
      'km / h',
      name: 'kmH',
      desc: '',
      args: [],
    );
  }

  /// `kilometers / h`
  String get kilometersH {
    return Intl.message(
      'kilometers / h',
      name: 'kilometersH',
      desc: '',
      args: [],
    );
  }

  /// `miles / h`
  String get milesH {
    return Intl.message(
      'miles / h',
      name: 'milesH',
      desc: '',
      args: [],
    );
  }

  /// `HUD on`
  String get hudOn {
    return Intl.message(
      'HUD on',
      name: 'hudOn',
      desc: '',
      args: [],
    );
  }

  /// `HUD off`
  String get hudOff {
    return Intl.message(
      'HUD off',
      name: 'hudOff',
      desc: '',
      args: [],
    );
  }

  /// `HUD view`
  String get hudView {
    return Intl.message(
      'HUD view',
      name: 'hudView',
      desc: '',
      args: [],
    );
  }

  /// `Rate 5 stars`
  String get rateFiveStars {
    return Intl.message(
      'Rate 5 stars',
      name: 'rateFiveStars',
      desc: '',
      args: [],
    );
  }

  /// `ver. 0.0.0.1`
  String get version {
    return Intl.message(
      'ver. 0.0.0.1',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get english {
    return Intl.message(
      'English',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `Russian`
  String get russian {
    return Intl.message(
      'Russian',
      name: 'russian',
      desc: '',
      args: [],
    );
  }

  /// `Belarus`
  String get belarus {
    return Intl.message(
      'Belarus',
      name: 'belarus',
      desc: '',
      args: [],
    );
  }

  /// `Ukraine`
  String get ukraine {
    return Intl.message(
      'Ukraine',
      name: 'ukraine',
      desc: '',
      args: [],
    );
  }

  /// `Deutsch`
  String get deutsch {
    return Intl.message(
      'Deutsch',
      name: 'deutsch',
      desc: '',
      args: [],
    );
  }

  /// `Buy Full Versions (ADS off)`
  String get buttonBuyFull {
    return Intl.message(
      'Buy Full Versions (ADS off)',
      name: 'buttonBuyFull',
      desc: '',
      args: [],
    );
  }

  /// `Rate 5 stars`
  String get buttonRate {
    return Intl.message(
      'Rate 5 stars',
      name: 'buttonRate',
      desc: '',
      args: [],
    );
  }

  /// `save changes`
  String get saveChanges {
    return Intl.message(
      'save changes',
      name: 'saveChanges',
      desc: '',
      args: [],
    );
  }

  /// `Buy Full Version`
  String get buyFullVersion {
    return Intl.message(
      'Buy Full Version',
      name: 'buyFullVersion',
      desc: '',
      args: [],
    );
  }

  /// `ADS off`
  String get adsOff {
    return Intl.message(
      'ADS off',
      name: 'adsOff',
      desc: '',
      args: [],
    );
  }

  /// `You can cancel your subscription in 'Settings'> Apple ID at any time, but at least one day before the renewal date. \n\nThe subscription will automatically renew until you cancel it`
  String get buyDescription {
    return Intl.message(
      'You can cancel your subscription in \'Settings\'> Apple ID at any time, but at least one day before the renewal date. \n\nThe subscription will automatically renew until you cancel it',
      name: 'buyDescription',
      desc: '',
      args: [],
    );
  }

  /// `Terms of Service and Pryvisy Policy`
  String get termsOfService {
    return Intl.message(
      'Terms of Service and Pryvisy Policy',
      name: 'termsOfService',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get defaultLanguage {
    return Intl.message(
      'English',
      name: 'defaultLanguage',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'ru'),
      Locale.fromSubtags(languageCode: 'ru', countryCode: 'BY'),
      Locale.fromSubtags(languageCode: 'uk', countryCode: 'UA'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
