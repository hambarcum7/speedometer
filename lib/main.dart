import 'package:shared_preferences/shared_preferences.dart';
import 'package:speedometer/provider/provider_gps.dart';
import 'package:speedometer/provider/provider_localization.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart' as ads;
import 'package:speedometer/ui/add_google/add_google.dart';

import 'export.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final initFuture = ads.MobileAds.instance.initialize();
  final futureSharedPreferences = SharedPreferences.getInstance();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (c) => ProviderAdMob(initFuture),
        ),
        ChangeNotifierProvider(
          create: (c) => ProviderLocalization(),
        ),
        // ChangeNotifierProvider(
        //   create: (c) => ProviderSetting(futureSharedPreferences),
        // ),
        ChangeNotifierProvider(
          create: (c) => ProviderGps(futureSharedPreferences),
        ),
      ],
      builder: (c, _) => const SpeedMain(),
    ),
  );
}

class SpeedMain extends StatelessWidget {
  const SpeedMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        colorScheme: kTheme.colorScheme,
        scaffoldBackgroundColor: kTheme.colorScheme.background,
        primaryColor: kTheme.colorScheme.primary,
        accentColor: kTheme.colorScheme.secondary,
      ),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      routes: {
        ScreenSpeed.id: (c) => const ScreenSpeed(),
        ScreenSplash.id: (c) => const ScreenSplash(),
        ScreenSettings.id: (c) => const ScreenSettings(),
        ScreenHud.id: (c) => const ScreenHud(),
        ScreenBuy.id: (c) => const ScreenBuy(),
        ScreenLanguage.id: (c) => const ScreenLanguage(),
      },
      initialRoute: ScreenSpeed.id,
    );
  }
}
