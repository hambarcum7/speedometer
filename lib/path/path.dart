// ignore: camel_case_types
class kPath {
  static const adb = 'assets/images/adb.png';
  static const buyBackImagePng = 'assets/images/buy_back_image.png';
  static const buyBackImagePngVertical =
      'assets/images/buy_back_image_vertical.png';
  static const arrowBack = 'assets/images/arrow_back.svg';
  static const arrowRight = 'assets/images/arrow_right.svg';
  static const arrowTopWithRect = 'assets/images/arrow_top_with_rect.svg';
  static const checkMarkSharp = 'assets/images/checkmark_sharp.svg';
  static const closeAdb = 'assets/images/close_adb.svg';
  static const closeSmall = 'assets/images/close_small.svg';
  static const svgMenu = 'assets/images/menu.svg';
  static const adsOff = 'assets/images/ads_off.svg';
}
