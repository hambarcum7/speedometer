import 'dart:async';
import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:speedometer/export.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';


class ProviderGps extends ChangeNotifier {
  Future<SharedPreferences> futureSharedPreferences;

  ProviderGps(this.futureSharedPreferences) {
    _stopwatch = Stopwatch();
    _location = Location();
    _stopwatch.start();
  }

  static const keyTripMeter = 'keyTripMeter';

  double maxSpeed = 0;
  double averageSpeed = 0;
  double speed = 0;
  double _tripMeter = 0;

  int _lastTime = 0;
  int secondTripMeter = 0;
  double _lastSpeed = 0;

  late Location _location;
  late StreamSubscription _locationSub;
  late Stopwatch _stopwatch;
  late Timer _timer;
  

  void init() async {
    await _location.requestPermission();
    streamTimer();
    _stopwatch.start();
  }

  double get tripMeterKm => _tripMeter / 1000; // trip meter in km
  double get tripMeterM => tripMeterKm / 1.609344;
  double get speedKm => speed * 3600 / 1000; // Speed in km/h
  double get speedM => speedKm / 1.609344;
  double get avgKm => averageSpeed * 3600 / 1000; // averageSpeed in km/h

  int get _tripTime => _stopwatch.elapsed.inSeconds + secondTripMeter;

  String get tripTime {
    var dateTime = DateTime(0, 0, 0, 0, 0, _tripTime);
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  void streamTimer() {
    Stream.periodic(
      const Duration(seconds: 1),
      (i) {},
    ).forEach((element) {
      notifyListeners();
      futureSharedPreferences
          .then((value) => value.setInt(keyTripMeter, secondTripMeter));
      getSpeed();
    });
  }

  String get time => DateFormat('hh:mm:ss').format(DateTime.now()).toString();

  Future<void> getSpeed([LocationData? data]) async {
    data ??= await _location.getLocation();
    speed = data.speed ?? 0;
    if (speed > 1) {
      maxSpeed = max(maxSpeed, speedKm);
      final sm = ((speed - _lastSpeed) * (_tripTime - _lastTime) / 1000).abs();
      _tripMeter += sm;
      averageSpeed = (_tripMeter / _tripTime);
    }
    _lastTime = _tripTime;
    _lastSpeed = speed;

    // print('Speed : $speed');
    // print('maxSpeed : $maxSpeed');
    print('averageSpeed : $averageSpeed');
    // print('tripMeter : $_tripMeter');
    print('tripMeterKm--------- : $tripMeterKm');
    print('avgKm------------ : $avgKm');
    print('speedKm--------------- : $speedKm');
    
    notifyListeners();
  }
  

  void finish() {
    _stopwatch.stop();
    _locationSub.cancel();
    _timer.cancel();
  }

  void reset() {
    maxSpeed = 0;
    averageSpeed = 0;
    speed = 0;
    _tripMeter = 0;
    _lastTime = 0;
    _lastSpeed = 0;

    _stopwatch.reset();
  }
}