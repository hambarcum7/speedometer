import 'package:speedometer/data/models/model_localization.dart';
import 'package:speedometer/export.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProviderLocalization extends ChangeNotifier {
  static const key = 'ProviderLocalization';
  SharedPreferences? _sharedPreferences;
  final ModelLocalization _modelLocalization = ModelLocalization();
  ProviderLocalization() {
    getSharedPreferences();
  }

  Future<void> getSharedPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    var index = _sharedPreferences?.getInt(key) ?? 0;
    load(index);
  }

  void setToSharedPreferences() {
    _sharedPreferences?.setInt(key, getIndexCurrentLocale());
    notifyListeners();
  }

  int getIndexCurrentLocale() {
    var data = Intl.getCurrentLocale();
    return _modelLocalization.supportedLocales.indexOf(data);
  }

  void saveDefaultLanguage() {
    _modelLocalization.defaultLanguage = getIndexCurrentLocale();
  }

  void loadDefaultLanguage() {
    load(_modelLocalization.defaultLanguage);
  }

  void load(int index) async {
    if (index == 0) {
      await S.load(const Locale('ru', ''));
    } else if (index == 1) {
      await S.load(const Locale('en', ''));
    } else if (index == 2) {
      await S.load(const Locale('ru', 'BY'));
    } else if (index == 3) {
      await S.load(const Locale('uk', 'Ua'));
    } else {
      await S.load(const Locale('de', ''));
    }
    notifyListeners();
  }
}
