import 'package:speedometer/export.dart';

// ignore: camel_case_types
class kTheme {
  static const colorScheme = ColorScheme.dark(
    background: Color(0xff000000),
    onBackground: Color(0xffFFFFFF),
    primary: Color(0xffFFFFFF),
    secondary: Color.fromRGBO(0, 148, 42, 1),
    onSecondary: Color(0xffFFFFFF),
    onPrimary: Color(0xff222222),
    primaryVariant: Color(0xff2B2B2B),
    secondaryVariant: Color(0xffD5D5D5),
    surface: Color(0xff202020),
    onSurface: Color(0xff999999),
  );
  static var textStyle = kTextStyle();
}

// ignore: camel_case_types
class kTextStyle {
  TextStyle get ald17onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 17,
        fontWeight: FontWeight.normal,
        height: 1.2941176470588236,
      );
  TextStyle get ald140onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 140,
        fontWeight: FontWeight.normal,
        height: 0.8571428571428571,
      );
  TextStyle get ald20onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 20,
        fontWeight: FontWeight.normal,
        height: 1.25,
      );
  TextStyle get ald28onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 28,
        fontWeight: FontWeight.normal,
        height: 1.2142857142857142,
      );
  TextStyle get ald15onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 15,
        fontWeight: FontWeight.normal,
        height: 1.3333333333333333,
      );
  TextStyle get ald15onPrimary => TextStyle(
        color: kTheme.colorScheme.onPrimary,
        fontFamily: 'Aldrich',
        fontSize: 15,
        fontWeight: FontWeight.normal,
        height: 1.3333333333333333,
      );
  TextStyle get ald36onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 36,
        fontWeight: FontWeight.normal,
        height: 1.3333333333333335,
      );
  TextStyle get ald11onGray => TextStyle(
        color: kTheme.colorScheme.surface,
        fontFamily: 'Aldrich',
        fontSize: 11,
        fontWeight: FontWeight.normal,
        height: 1.1818181818181819,
      );
  TextStyle get ald182onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 182,
        fontWeight: FontWeight.normal,
        height: 1,
      );
  TextStyle get ald34onBackground => TextStyle(
        color: kTheme.colorScheme.onBackground,
        fontFamily: 'Aldrich',
        fontSize: 34,
        fontWeight: FontWeight.normal,
        height: 1,
      );
  TextStyle get ald28onPrimary => TextStyle(
        color: kTheme.colorScheme.onPrimary,
        fontFamily: 'Aldrich',
        fontSize: 28,
        fontWeight: FontWeight.normal,
        height: 1.2142857142857142,
      );
  TextStyle get ald13onPrimary => const TextStyle(
        color: Color.fromRGBO(147, 147, 147, 1),
        fontFamily: 'Aldrich',
        fontSize: 13,
        fontWeight: FontWeight.normal,
        height: 1.3846153846153846,
      );
  TextStyle get rubik28onPrimary => TextStyle(
        color: kTheme.colorScheme.onPrimary,
        fontFamily: 'Rubik',
        fontSize: 15,
        fontWeight: FontWeight.normal,
        height: 1.3333333333333333,
      );
  TextStyle get rubik28onPrimaryG => GoogleFonts.rubik(
        color: kTheme.colorScheme.onPrimary,
        fontSize: 15,
        height: 1.3333333333333333,
        fontWeight: FontWeight.normal,
      );
}
