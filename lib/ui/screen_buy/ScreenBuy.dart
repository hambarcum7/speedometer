import 'package:speedometer/export.dart';
import 'build_app_bar.dart';

class ScreenBuy extends StatelessWidget {
  static const id = 'ScreenBuy';
  const ScreenBuy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.of(context).orientation == Orientation.portrait
        ? buildPortrait(context)
        : buildLandscape(context);
  }
}

Widget buildPortrait(BuildContext context) {
  return Scaffold(
    appBar: buildAppBar(context, height: 265),
    backgroundColor: kTheme.colorScheme.primary,
    body: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Flexible(child: SizedBox(height: 48)),
          Text(
            S.of(context).buyFullVersion,
            style: kTheme.textStyle.ald28onPrimary,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10),
          Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Positioned(
                bottom: 4,
                child: Container(
                  width: context
                              .read<ProviderLocalization>()
                              .getIndexCurrentLocale() !=
                          1
                      ? 300
                      : null,
                  child: SvgPicture.asset(
                    kPath.adsOff,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Text(
                S.of(context).adsOff,
                style: kTheme.textStyle.ald28onPrimary,
              ),
            ],
          ),
          const Flexible(
            child: SizedBox(height: 48),
          ),
          Text(
            S.of(context).buyDescription,
            style: kTheme.textStyle.rubik28onPrimaryG,
          ),
          const Spacer(flex: 2),
          WrapperButton(
            data: S.of(context).buttonBuyFull,
            onTap: () {/*todo buy full*/},
            color: kTheme.colorScheme.secondary,
            textStyle: kTheme.textStyle.ald17onBackground,
          ),
          const SizedBox(height: 32),
          Text(
            S.of(context).termsOfService,
            style: kTheme.textStyle.ald13onPrimary,
            textAlign: TextAlign.center,
          ),
          const Spacer(),
        ],
      ),
    ),
  );
}

Widget buildLandscape(BuildContext context) {
  return Scaffold(
    backgroundColor: kTheme.colorScheme.primary,
    body: Row(
      children: [
        Expanded(
          flex: 193,
          child: Container(
            child: Padding(
              padding: EdgeInsets.only(
                top: 24,
                left: MediaQuery.of(context).size.width * 0.05,
                right: MediaQuery.of(context).size.width * 0.05,
                bottom: 8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Flexible(child: SizedBox(height: 48)),
                  Text(
                    S.of(context).buyFullVersion,
                    style: kTheme.textStyle.ald28onPrimary,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 10),
                  Stack(
                    fit: StackFit.passthrough,
                    alignment: Alignment.bottomCenter,
                    children: [
                      Positioned(
                        bottom: 4,
                        child: Container(
                          width: context
                                      .read<ProviderLocalization>()
                                      .getIndexCurrentLocale() !=
                                  1
                              ? 300
                              : null,
                          child: SvgPicture.asset(
                            kPath.adsOff,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Text(
                        S.of(context).adsOff,
                        style: kTheme.textStyle.ald28onPrimary,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  const Flexible(child: SizedBox(height: 48)),
                  Text(
                    S.of(context).buyDescription,
                    style: kTheme.textStyle.rubik28onPrimaryG,
                  ),
                  const Spacer(flex: 2),
                  WrapperButton(
                    data: S.of(context).buttonBuyFull,
                    onTap: () {/*todo buy full*/},
                    color: kTheme.colorScheme.secondary,
                    textStyle: kTheme.textStyle.ald17onBackground,
                  ),
                  const SizedBox(height: 32),
                  Text(
                    S.of(context).termsOfService,
                    style: kTheme.textStyle.ald13onPrimary,
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 100,
          child: Stack(
            children: [
              Image.asset(
                kPath.buyBackImagePngVertical,
                width: double.infinity,
                fit: BoxFit.cover,
                height: double.infinity,
              ),
              Container(
                height: 100,
                child: AppBar(
                  automaticallyImplyLeading: false,
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  actions: [
                    IconButton(
                      icon: SvgPicture.asset(kPath.closeSmall),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
