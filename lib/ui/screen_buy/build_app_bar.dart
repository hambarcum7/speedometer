import 'package:speedometer/export.dart';

PreferredSizeWidget buildAppBar(BuildContext context,
    {required double height}) {
  return PreferredSize(
    preferredSize: Size(double.infinity, height),
    child: Stack(
      children: [
        Image.asset(
          kPath.buyBackImagePng,
          width: double.infinity,
          fit: BoxFit.fitWidth,
        ),
        Container(
          height: 100,
          child: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: Colors.transparent,
            actions: [
              IconButton(
                icon: SvgPicture.asset(kPath.closeSmall),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
