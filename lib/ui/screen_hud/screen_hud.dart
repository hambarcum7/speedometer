import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:speedometer/export.dart';
import 'package:speedometer/generated/l10n.dart';
import 'package:speedometer/path/path.dart';
import 'package:speedometer/provider/provider_gps.dart';
import 'package:speedometer/theme/theme.dart';

class ScreenHud extends StatelessWidget {
  static const id = 'ScreenHud';
  const ScreenHud({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: kTheme.colorScheme.background,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(kPath.closeSmall),
          )
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            orientation == Orientation.portrait
                ? const Spacer()
                : const SizedBox(height: 16),
            Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationX(math.pi),
              child: Text(
                orientation == Orientation.portrait
                    ? S.of(context).kmH
                    : S.of(context).milesH,
                style: kTheme.textStyle.ald34onBackground,
              ),
            ),
            Expanded(
              child: Transform(
                alignment: Alignment.center,
                transform: Matrix4.rotationX(math.pi),
                child: FittedBox(
                  child: Text(
                    context.watch<ProviderGps>().speedKm.toStringAsFixed(1),
                    style: kTheme.textStyle.ald182onBackground.copyWith(
                      fontSize: orientation == Orientation.portrait ? 82 : 232,
                    ),
                  ),
                ),
              ),
            ),
            orientation == Orientation.portrait
                ? const Spacer()
                : const SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
