import 'package:speedometer/export.dart';

class ListLanguage extends StatefulWidget {
  final Function(int i)? getIndex;
  final Function(int i)? saveChange;
  const ListLanguage({Key? key, this.getIndex, this.saveChange})
      : super(key: key);

  @override
  _ListLanguageState createState() => _ListLanguageState();
}

class _ListLanguageState extends State<ListLanguage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    final kString = S.of(context);
    index = context.read<ProviderLocalization>().getIndexCurrentLocale();
    final listLaunguage = [
      kString.russian,
      kString.english,
      kString.belarus,
      kString.ukraine,
      kString.deutsch,
    ];
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: listLaunguage.length + 1,
      itemBuilder: (c, i) => listLaunguage.length > i
          ? ListTile(
              contentPadding: const EdgeInsets.only(left: 24, right: 16),
              leading: Text(
                listLaunguage[i],
                style: kTheme.textStyle.ald17onBackground
                    .copyWith(color: Colors.white),
              ),
              trailing:
                  index == i ? SvgPicture.asset(kPath.checkMarkSharp) : null,
              onTap: () {
                index = i;
                context.read<ProviderLocalization>().load(index);
                setState(() {});
                // if (widget.getIndex != null) widget.getIndex!(i);
              },
            )
          : Container(
              height: 60,
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
              child: WrapperButton(
                data: S.of(context).saveChanges,
                onTap: () {
                  if (widget.saveChange != null) widget.saveChange!(index);
                  context.read<ProviderLocalization>().setToSharedPreferences();
                },
                color: kTheme.colorScheme.background,
                textStyle: kTheme.textStyle.ald17onBackground,
                borderSideColor: kTheme.colorScheme.onBackground,
              ),
            ),
    );
  }
}
