import 'package:speedometer/export.dart';
import 'list_language.dart';

class ScreenLanguage extends StatelessWidget {
  static const id = 'Screen Language';
  const ScreenLanguage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      body: WrapperMaxWithInLandscape(
        child: Column(
          children: [
            Expanded(
              child: ListLanguage(
                getIndex: (i) {
                  print('index = $i');
                },
                saveChange: (i) {
                  print('Save Change = $i');
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(
              height: orientation == Orientation.portrait ? 80 : 0,
            ),
          ],
        ),
      ),
      bottomSheet: orientation == Orientation.portrait
          ? WrapperAdb(
              onTap: () {},
            )
          : null,
    );
  }
}
