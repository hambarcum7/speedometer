import 'package:flutter/material.dart';
import 'package:speedometer/export.dart';

typedef OnToggle = void Function(int index);

// ignore: must_be_immutable
class ToggleSwitch extends StatefulWidget {
  /// Active background color
  final Color? activeBgColor;

  /// Active foreground color
  final Color? activeFgColor;

  /// Inactive background color
  final Color? inactiveBgColor;

  /// Inactive foreground color
  final Color? inactiveFgColor;

  /// List of labels
  final List<String> labels;

  /// List of icons
  final List<IconData>? icons;

  /// List of active foreground colors
  final List<Color>? activeBgColors;

  /// Minimum switch width
  final double minWidth;

  /// Minimum switch height
  final double minHeight;

  /// Widget's corner radius
  final double cornerRadius;

  /// Font size
  final double fontSize;

  /// Icon size
  final double iconSize;

  /// OnToggle function
  final OnToggle? onToggle;

  // Change selection on tap
  final bool changeOnTap;

  /// Initial label index
  int initialLabelIndex;

  ToggleSwitch({
    Key? key,
    required this.labels,
    this.activeBgColor,
    this.activeFgColor,
    this.inactiveBgColor,
    this.inactiveFgColor,
    this.onToggle,
    this.cornerRadius = 8.0,
    this.initialLabelIndex = 0,
    this.minWidth = 72.0,
    this.minHeight = 40.0,
    this.changeOnTap = true,
    this.icons,
    this.activeBgColors,
    this.fontSize = 14.0,
    this.iconSize = 17.0,
  }) : super(key: key);

  @override
  _ToggleSwitchState createState() => _ToggleSwitchState();
}

class _ToggleSwitchState extends State<ToggleSwitch>
    with AutomaticKeepAliveClientMixin<ToggleSwitch> {
  /// Active background color
  Color? activeBgColor;

  /// Active foreground color
  Color? activeFgColor;

  /// Inactive background color
  Color? inactiveBgColor;

  /// Inctive foreground color
  Color? inactiveFgColor;

  /// Maintain selection state.
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    /// Assigns active background color to default primary theme color if it's null/not provided.
    activeBgColor = widget.activeBgColor ?? Theme.of(context).primaryColor;

    /// Assigns active foreground color to default accent text theme color if it's null/not provided.
    activeFgColor = widget.activeFgColor ??
        Theme.of(context).accentTextTheme.bodyText1!.color;

    /// Assigns inactive background color to default disabled theme color if it's null/not provided.
    inactiveBgColor = widget.inactiveBgColor ?? Theme.of(context).disabledColor;

    /// Assigns inactive foreground color to default text theme color if it's null/not provided.
    inactiveFgColor =
        widget.inactiveFgColor ?? Theme.of(context).textTheme.bodyText1!.color;

    return Container(
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: inactiveBgColor,
      ),
      padding: const EdgeInsets.all(2),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(widget.labels.length * 2 - 1, (index) {
          final active = index ~/ 2 == widget.initialLabelIndex;

          final fgColor = active ? activeFgColor : inactiveFgColor;

          Color? bgColor = Colors.transparent;
          if (active) {
            bgColor = widget.activeBgColors == null
                ? activeBgColor
                : widget.activeBgColors![index ~/ 2];
          }
          if (index % 2 == 1) {
            final activeDivider =
                active || index ~/ 2 == widget.initialLabelIndex - 1;

            return Container(
              width: 1,
              color: activeDivider ? bgColor : Colors.white30,
              margin: EdgeInsets.symmetric(vertical: activeDivider ? 0 : 8),
            );
          } else {
            /// Returns switch item
            return GestureDetector(
              onTap: () => _handleOnTap(index ~/ 2),
              child: Container(
                padding: const EdgeInsets.only(left: 12.0, right: 12.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(color: bgColor),
                child: widget.icons == null
                    ? Text(
                        widget.labels[index ~/ 2],
                        style: kTheme.textStyle.ald15onBackground
                            .copyWith(color: fgColor),
                        overflow: TextOverflow.ellipsis,
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            widget.icons![index ~/ 2],
                            color: fgColor,
                            size: widget.iconSize >
                                    (_calculateWidth(widget.minWidth) / 3)
                                ? (_calculateWidth(widget.minWidth)) / 3
                                : widget.iconSize,
                          ),
                          Flexible(
                            child: Container(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                widget.labels[index ~/ 2],
                                style: TextStyle(
                                  color: fgColor,
                                  fontSize: widget.fontSize,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
              ),
            );
          }
        }),
      ),
    );
  }

  /// Handles selection
  void _handleOnTap(int index) async {
    if (widget.changeOnTap) {
      setState(() => widget.initialLabelIndex = index);
    }
    if (widget.onToggle != null) {
      widget.onToggle!(index);
    }
  }

  double _calculateWidth(double minWidth) {
    var totalLabels = widget.labels.length;

    var extraWidth = 0.10 * totalLabels;

    var screenWidth = MediaQuery.of(context).size.width;
    return (totalLabels + extraWidth) * widget.minWidth < screenWidth
        ? widget.minWidth
        : screenWidth / (totalLabels + extraWidth);
  }
}
