import 'package:speedometer/export.dart';
import 'package:flutter/cupertino.dart';
import 'package:speedometer/provider/provider_gps.dart';
import 'ToggleSwitch.dart' as toggle_switch;
import 'package:flutter/rendering.dart';

class ScreenSettings extends StatefulWidget {
  static const id = 'ScreenSettings';

  const ScreenSettings({Key? key}) : super(key: key);

  @override
  _ScreenSettingsState createState() => _ScreenSettingsState();
}

class _ScreenSettingsState extends State<ScreenSettings> {
  String text = '';
  String subject = '';
  List<String> imagePaths = [];
  late bool button2;
  final GlobalKey _globalKey = GlobalKey();
  @override
  void initState() {
    button2 = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final kString = S.of(context);

    return Scaffold(
      key: _globalKey,
      body: WrapperMaxWithInLandscape(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            WrapperAppBar(
              title: kString.settings,
              trailing: TextButton(
                onPressed: null,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: button2
                      ? const CircularProgressIndicator()
                      : const Icon(Icons.share),
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollViewLandscape(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        const SizedBox(height: 24),
                        Text(
                          kString.speedUnit,
                          style: kTheme.textStyle.ald11onGray.copyWith(
                            color: kTheme.colorScheme.onSurface,
                          ),
                          textAlign: TextAlign.start,
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        toggle_switch.ToggleSwitch(
                          initialLabelIndex: 0,
                          labels: [kString.kilometersH, kString.milesH],
                          onToggle: (index) {
                            print('switched to: $index');
                          },
                          activeBgColor: kTheme.colorScheme.primary,
                          activeFgColor: kTheme.colorScheme.onPrimary,
                          inactiveBgColor: kTheme.colorScheme.onPrimary,
                        ),
                        const SizedBox(height: 24),
                        Text(
                          kString.language,
                          style: kTheme.textStyle.ald11onGray.copyWith(
                            color: kTheme.colorScheme.onSurface,
                          ),
                          textAlign: TextAlign.start,
                        ),
                        const SizedBox(height: 12),
                        InkWell(
                          onTap: () async {
                            await Navigator.pushNamed(
                                context, ScreenLanguage.id);
                            context
                                .read<ProviderLocalization>()
                                .loadDefaultLanguage();
                            setState(() {});
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                kString.defaultLanguage,
                                style: kTheme.textStyle.ald20onBackground,
                              ),
                              SvgPicture.asset(kPath.arrowRight),
                            ],
                          ),
                        ),
                        orientation == Orientation.portrait
                            ? const Spacer()
                            : const SizedBox(height: 16),
                        const SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          height: 60,
                          child: WrapperButton(
                            data: kString.buyFullVersion,
                            onTap: () {
                              Navigator.pushNamed(context, ScreenBuy.id);
                            },
                            color: kTheme.colorScheme.secondary,
                            textStyle: kTheme.textStyle.ald17onBackground,
                          ),
                        ),
                        const SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          height: 60,
                          child: WrapperButton(
                            data: kString.rateFiveStars,
                            onTap: () {},
                            color: kTheme.colorScheme.background,
                            borderSideColor: kTheme.colorScheme.onBackground,
                            textStyle: kTheme.textStyle.ald17onBackground,
                          ),
                        ),
                        const SizedBox(height: 24),
                        Center(
                          child: Text(
                            kString.version,
                            textAlign: TextAlign.center,
                            style: kTheme.textStyle.ald13onPrimary.copyWith(
                              color: kTheme.colorScheme.onPrimary,
                            ),
                          ),
                        ),
                        const SizedBox(height: 110),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: orientation == Orientation.portrait
          ? WrapperAdb(
              onTap: () {},
            )
          : null,
    );
  }
}
