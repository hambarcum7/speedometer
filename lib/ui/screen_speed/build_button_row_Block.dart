import 'package:flutter/material.dart';
import 'package:speedometer/export.dart';
import 'package:speedometer/generated/l10n.dart';
import 'package:speedometer/provider/provider_gps.dart';
import 'package:speedometer/ui/widgets_common/button_black.dart';
import 'package:speedometer/ui/widgets_common/button_white.dart';

Padding buildButtonRowBlock(BuildContext context) {
  final kString = S.of(context);
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
    child: Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Expanded(
            child: SizedBox(
              height: 40,
              child: ButtonBlack(
                  onTap: () {
                    context.read<ProviderGps>().reset();
                  },
                  data: kString.reset),
            ),
          ),
          const SizedBox(width: 24),
          Expanded(
            child: SizedBox(
              height: 40,
              child: ButtonWhite(
                  onTap: () {
                    Navigator.pushNamed(context, ScreenHud.id);
                  },
                  data: kString.hud),
            ),
          ),
        ],
      ),
    ),
  );
}
