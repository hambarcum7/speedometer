import 'dart:io';
import 'dart:core';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:speedometer/export.dart';
import 'package:speedometer/provider/provider_gps.dart';
import 'package:speedometer/ui/add_google/add_google.dart';
import 'build_button_row_Block.dart';

class ScreenSpeed extends StatefulWidget {
  static const id = 'ScreenSpeed';
  const ScreenSpeed({Key? key}) : super(key: key);

  @override
  _ScreenSpeedState createState() => _ScreenSpeedState();
}

const int maxFailedLoadAttempts = 3;

class _ScreenSpeedState extends State<ScreenSpeed> {
  late BannerAd _bannerAd;
  static const AdRequest request = AdRequest(
    keywords: <String>['foo', 'bar'],
    contentUrl: 'http://foo.com/bar.html',
    nonPersonalizedAds: true,
  );
  InterstitialAd? _interstitialAd;
  int _numInterstitialLoadAttempts = 0;
  var _isBannerAdReady = false;

  @override
  void initState() {
    super.initState();

    MobileAds.instance.updateRequestConfiguration(
      RequestConfiguration(
        testDeviceIds: ['Simulator'],
      ),
    );

    _bannerAd = BannerAd(
      adUnitId: AdMobHelper.bannerAdUnitId,
      request: const AdRequest(),
      size: AdSize.banner,
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _isBannerAdReady = true;
          });
        },
        onAdFailedToLoad: (ad, err) {
          print('Failed to load a banner ad: ${err.message}');
          _isBannerAdReady = false;
          ad.dispose();
        },
      ),
    );

    void _createInterstitialAd() async {
      await InterstitialAd.load(
        adUnitId: Platform.isAndroid
            ? 'ca-app-pub-3940256099942544/1033173712'
            : 'ca-app-pub-3940256099942544/4411468910',
        request: request,
        adLoadCallback: InterstitialAdLoadCallback(
          onAdLoaded: (InterstitialAd ad) {
            print('$ad loaded 1######');
            _interstitialAd = ad;
            _numInterstitialLoadAttempts = 0;
            _interstitialAd!.setImmersiveMode(true);
            _interstitialAd!.show();
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('InterstitialAd failed to load: $error. 2####');
            _numInterstitialLoadAttempts += 1;
            _interstitialAd = null;
            if (_numInterstitialLoadAttempts <= maxFailedLoadAttempts) {
              _createInterstitialAd();
            }
          },
        ),
      );
    }

    _createInterstitialAd();

    _bannerAd.load();
    context.read<ProviderGps>().init();

    context
        .read<ProviderLocalization>()
        .getSharedPreferences()
        .then((value) => setState(() {}));
  }

  @override
  void dispose() {
    super.dispose();
    _interstitialAd?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return orientation == Orientation.portrait
        ? buildPortrait(context)
        : buildLandscape(context);
  }

  Widget buildPortrait(BuildContext context) {
    final kString = S.of(context);
    final orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () async {
              await Navigator.pushNamed(context, ScreenSettings.id);
              context.read<ProviderLocalization>().loadDefaultLanguage();
              setState(() {});
            },
            icon: SvgPicture.asset(kPath.svgMenu),
          ),
        ],
        backgroundColor: kTheme.colorScheme.background,
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
          top: 2,
          bottom: 80,
        ),
        child: FittedBox(
          fit: BoxFit.fitHeight,
          alignment: Alignment.center,
          child: Column(
            children: [
              SpeedWidget(
                speed: context.watch<ProviderGps>().speedKm.toInt(),
              ),
              Text(
                context.watch<ProviderGps>().speedKm.toStringAsFixed(0),
                style: kTheme.textStyle.ald140onBackground, //todo set speed
                textAlign: TextAlign.center,
              ),
              Text(
                S.of(context).kmH,
                style: kTheme.textStyle.ald20onBackground,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 40),
              WrapperListTile(
                leading: kString.maxSpeed,
                trailing:
                    context.watch<ProviderGps>().maxSpeed.toStringAsFixed(0),
              ),
              WrapperListTile(
                leading: kString.averageSpeed,
                trailing: context.watch<ProviderGps>().avgKm.toStringAsFixed(0),
              ),
              WrapperListTile(
                leading: kString.tripMeter,
                trailing:
                    context.watch<ProviderGps>().tripMeterKm.toStringAsFixed(1),
              ),
              WrapperListTile(
                leading: kString.tripTime,
                trailing: context.watch<ProviderGps>().tripTime,
              ),
              WrapperListTile(
                key: const Key('time'),
                leading: kString.time,
                trailing: context.watch<ProviderGps>().time,
              ),
              buildButtonRowBlock(context),
            ],
          ),
        ),
      ),
      bottomSheet: Container(
        height: null,
        child: WrapperAdb(
          onTap: () {},
        ),
      ),
    );
  }

  Widget buildLandscape(BuildContext context) {
    final kString = S.of(context);
    final orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.30,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  InkWell(
                    onTap: () {
                      print('reset');
                    },
                    child: Text(
                      S.of(context).reset,
                      style: kTheme.textStyle.ald13onPrimary.copyWith(
                        color: kTheme.colorScheme.onBackground,
                      ),
                    ),
                  ),
                  const SizedBox(height: 16),
                  WrapperListTileColumn(
                      leading: kString.maxSpeed, trailing: '135'),
                  WrapperListTileColumn(
                      leading: kString.averageSpeed, trailing: '44'),
                  WrapperListTileColumn(
                      leading: kString.tripMeter, trailing: '230'),
                ],
              ),
            ),
          ),
          Center(
            child: FittedBox(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SpeedWidget(
                      speed: context.watch<ProviderGps>().speedKm.toInt(),
                    ),
                    const SizedBox(height: 24),
                    Text(
                      context
                          .watch<ProviderGps>()
                          .speed
                          .toString(), //todo set speed
                      style: kTheme.textStyle.ald140onBackground,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      S.of(context).kmH,
                      style: kTheme.textStyle.ald20onBackground,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
          FittedBox(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.3,
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () async {
                      await Navigator.pushNamed(context, ScreenSettings.id);
                      context
                          .read<ProviderLocalization>()
                          .loadDefaultLanguage();
                      setState(() {});
                    },
                    child: SvgPicture.asset(kPath.svgMenu),
                  ),
                  const SizedBox(height: 16),
                  WrapperListTileColumn(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    leading: kString.tripTime,
                    trailing: context.watch<ProviderGps>().tripTime,
                  ),
                  WrapperListTileColumn(
                    leading: kString.time,
                    trailing: context.watch<ProviderGps>().time,
                    crossAxisAlignment: CrossAxisAlignment.end,
                  ),
                  const SizedBox(height: 16),
                  Container(
                    height: 40,
                    width: 160,
                    child: ButtonWhite(
                      data: kString.hud,
                      onTap: () {
                        Navigator.pushNamed(context, ScreenHud.id);
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
