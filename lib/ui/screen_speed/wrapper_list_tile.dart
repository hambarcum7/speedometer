import 'package:flutter/material.dart';
import 'package:speedometer/theme/theme.dart';

class WrapperListTile extends StatelessWidget {
  final String leading;
  final String trailing;
  const WrapperListTile(
      {Key? key, required this.leading, required this.trailing})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        leading: Text(
          leading,
          style: kTheme.textStyle.ald17onBackground,
        ),
        trailing: Text(
          trailing,
          style: kTheme.textStyle.ald28onBackground,
        ),
      ),
    );
  }
}

class WrapperListTileColumn extends StatelessWidget {
  final String leading;
  final String trailing;
  final CrossAxisAlignment? crossAxisAlignment;
  const WrapperListTileColumn(
      {Key? key,
      required this.leading,
      required this.trailing,
      this.crossAxisAlignment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16),
      //width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: crossAxisAlignment ?? CrossAxisAlignment.start,
        children: [
          Text(
            leading,
            style: kTheme.textStyle.ald17onBackground,
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            trailing,
            style: kTheme.textStyle.ald28onBackground,
          ),
        ],
      ),
    );
  }
}
