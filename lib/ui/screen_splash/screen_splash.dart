import 'package:flutter/material.dart';
import 'package:speedometer/export.dart';
import 'package:speedometer/generated/l10n.dart';
import 'package:speedometer/theme/theme.dart';

class ScreenSplash extends StatelessWidget {
  static const id = 'ScreenSplash';
  const ScreenSplash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
            child: Text(
              S.of(context).speedometer,
              style: orientation == Orientation.portrait
                  ? kTheme.textStyle.ald36onBackground
                  : kTheme.textStyle.ald36onBackground.copyWith(fontSize: 56),
              textAlign: TextAlign.center,
            ),
          ),
          Text(
            S.of(context).loading,
            style: kTheme.textStyle.ald20onBackground,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
