import 'package:flutter/material.dart';
import 'package:speedometer/theme/theme.dart';

class ButtonBlack extends StatelessWidget {
  final Function() onTap;
  final String data;
  const ButtonBlack({Key? key, required this.onTap, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        backgroundColor:
            MaterialStateProperty.all(kTheme.colorScheme.background),
        textStyle:
            MaterialStateProperty.all(kTheme.textStyle.ald15onBackground),
        side: MaterialStateProperty.all(
          BorderSide(color: kTheme.colorScheme.onBackground),
        ),
      ),
      onPressed: onTap,
      child: Text(data),
    );
  }
}
