import 'package:flutter/material.dart';
import 'package:speedometer/theme/theme.dart';

class ButtonWhite extends StatelessWidget {
  final Function() onTap;
  final String data;
  const ButtonWhite({Key? key, required this.onTap, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(kTheme.colorScheme.primary),
        textStyle: MaterialStateProperty.all(kTheme.textStyle.ald15onPrimary),
      ),
      onPressed: onTap,
      child: Text(
        data,
        style: kTheme.textStyle.ald15onPrimary,
      ),
    );
  }
}
