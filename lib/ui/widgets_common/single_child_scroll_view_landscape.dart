import 'package:speedometer/export.dart';

class SingleChildScrollViewLandscape extends StatelessWidget {
  SingleChildScrollViewLandscape({
    Key? key,
    required this.child,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return orientation == Orientation.landscape
        ? SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: child,
          )
        : child;
  }
}
