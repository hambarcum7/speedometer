import 'package:speedometer/export.dart';

class SpeedWidget extends StatefulWidget {
  const SpeedWidget({Key? key, required this.speed}) : super(key: key);
  final int speed;
  @override
  _SpeedWidgetState createState() => _SpeedWidgetState();
}

class _SpeedWidgetState extends State<SpeedWidget> {
  @override
  Widget build(BuildContext context) {
    const width = 25.0;
    const height = 31.0;
    const k = 20;
    return Center(
      child: FittedBox(
        child: Container(
          margin: const EdgeInsets.only(bottom: 32, left: 0, right: 0),
          child: Row(children: [
            const SizedBox(width: 16),
            ...List.generate(
              11,
              (index) => Column(
                children: [
                  Text(
                    (20 + index * 20).toString(),
                    style: kTheme.textStyle.ald13onPrimary.copyWith(
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 3),
                    transform: Matrix4.skewX(-0.4),
                    height: height,
                    width: width,
                    decoration: BoxDecoration(
                        color: ((widget.speed) ~/ k) > index
                            ? kTheme.colorScheme.onBackground
                            : kTheme.colorScheme.onBackground.withOpacity(0.3)),
                    child: (widget.speed ~/ k) == index
                        ? Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: height,
                              width: width * ((widget.speed % 20) / 20),
                              color: kTheme.colorScheme.onBackground,
                            ),
                          )
                        : null,
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
