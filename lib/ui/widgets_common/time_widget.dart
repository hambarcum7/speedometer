import 'package:flutter/material.dart';

class TimeWidget extends StatefulWidget {
  const TimeWidget({Key? key}) : super(key: key);

  @override
  _TimeWidgetState createState() => _TimeWidgetState();
}

class _TimeWidgetState extends State<TimeWidget> {
  @override
  void initState() {
    DateTime;
    super.initState();
  }

  Stream<DateTime> stream() async* {
    for (;;) {
      yield DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder<DateTime>(
          initialData: DateTime.now(),
          stream: Stream<DateTime>.periodic(
              const Duration(seconds: 1), (i) => DateTime.now()),
          // ignore: avoid_types_as_parameter_names
          builder: (BuildContext context, AsyncSnapshot<DateTime> snapshot) {
            final data = snapshot.data;
            return data == null
                ? const Text('error get Data')
                : Text(
                    '${data.hour.toString()}:${data.minute.toString()}:${data.second.toString()}:${data.microsecond.toString()}');
          }),
    );
  }
}
