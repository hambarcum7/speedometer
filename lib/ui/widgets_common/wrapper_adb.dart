import 'package:flutter/material.dart';

import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'package:speedometer/export.dart';
import 'package:speedometer/ui/add_google/add_google.dart';

class WrapperAdb extends StatefulWidget {
  final void Function() onTap;
  const WrapperAdb({Key? key, required this.onTap}) : super(key: key);

  @override
  _WrapperAdbState createState() => _WrapperAdbState();
}

class _WrapperAdbState extends State<WrapperAdb> {
  BannerAd? banner;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adMob = context.read<ProviderAdMob>();
    adMob.initialization.then(
      (value) {
        setState(
          () {
            banner = BannerAd(
              adUnitId: AdMobHelper.bannerAdUnitId,
              size: AdSize.banner,
              request: const AdRequest(),
              listener: context.read<ProviderAdMob>().addListened1,
            )..load();
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: banner == null
          ? const SizedBox(
              height: 1,
            )
          : Container(
              color: kTheme.colorScheme.background,
              height: 80,
              width: double.infinity,
              child: AdWidget(
                ad: banner!,
              ),
            ),
    );
  }
}
