import '../../export.dart';

class WrapperAppBar extends StatefulWidget {
  final Widget? leading;

  final String title;
  final Widget trailing;
  const WrapperAppBar(
      {Key? key, this.leading, required this.title, required this.trailing})
      : super(key: key);

  @override
  _WrapperAppBarState createState() => _WrapperAppBarState();
}

class _WrapperAppBarState extends State<WrapperAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: kTheme.colorScheme.background,
      title: Text(
        widget.title,
        style: kTheme.textStyle.ald15onBackground,
      ),
      leading: widget.leading ??
          IconButton(
            icon: SvgPicture.asset(kPath.arrowBack),
            onPressed: () {
              context.read<ProviderLocalization>().loadDefaultLanguage();
              Navigator.pop(context);
            },
          ),
      actions: [
        widget.trailing,
      ],
    );
  }
}
