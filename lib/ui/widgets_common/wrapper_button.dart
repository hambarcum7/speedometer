import 'package:flutter/widgets.dart';
import 'package:speedometer/export.dart';

class WrapperButton extends StatelessWidget {
  final Color color;
  final Color? borderSideColor;
  final TextStyle textStyle;
  final String data;
  final Function onTap;
  const WrapperButton({
    Key? key,
    required this.data,
    required this.onTap,
    required this.color,
    required this.textStyle,
    this.borderSideColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: TextButton(
        onPressed: () {
          context.read<ProviderLocalization>().saveDefaultLanguage();
          onTap();
        },
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
              side: BorderSide(
                color: borderSideColor ?? Colors.transparent,
                width: 1,
              ),
            ),
          ),
          backgroundColor: MaterialStateProperty.all(
            color,
          ), //kTheme.colorSheme.secondary
          padding: MaterialStateProperty.all(
            const EdgeInsets.symmetric(horizontal: 16),
          ),
        ),
        child: FittedBox(
          child: Text(
            data,
            style: textStyle,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
