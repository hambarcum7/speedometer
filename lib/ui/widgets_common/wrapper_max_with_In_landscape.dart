import 'package:speedometer/export.dart';

class WrapperMaxWithInLandscape extends StatelessWidget {
  final double ratioWidth;
  final Widget child;

  const WrapperMaxWithInLandscape({
    Key? key,
    this.ratioWidth = 0.7,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Container(
      width: orientation == Orientation.portrait
          ? MediaQuery.of(context).size.width
          : MediaQuery.of(context).size.width * 0.7,
      padding: orientation == Orientation.landscape
          ? const EdgeInsets.symmetric(horizontal: 32)
          : null,
      child: child,
    );
  }
}
